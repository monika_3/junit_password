/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java_password;

import junit_password.Password;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author monika
 */
public class java_passwordTest {

    public java_passwordTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {

    }

    /**
     * Test of isStrong method, of class junit_password.
     *
     * @throws junit_password.Password.ExceptionOutOfRange
     */
    @Test
    public void testIsStrong() throws Password.ExceptionOutOfRange {
        System.out.println("testIsStrong");

        Password password = new Password("HoLa7373");
        assertFalse(password.isStrong());

        System.out.println("Password " + Password.getPassword());
    }

    /**
     * Test of Password method, of class junit_password.
     *
     * @throws junit_password.Password.ExceptionOutOfRange
     */
    @Test
    public void testPassword() throws Password.ExceptionOutOfRange {
        System.out.println("testPassword");
        Password pwd = new Password("Teest123");
        String result = pwd.getPassword();
        assertEquals("Teest123", result);
        System.out.println("Password " + result);
    }

}
