/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package junit_password;

import java.security.SecureRandom;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author monika
 */
public class Password {

    protected static int longitud = 10;
    private static String password;

    public Password(String password) throws ExceptionOutOfRange {
        if (!(password.length() > 6) || !(password.length() < 10)) {
            throw new ExceptionOutOfRange("Out Of Range");
        } else{
            this.password = password;
        }
    }

    public class ExceptionOutOfRange extends Exception {

        public ExceptionOutOfRange(String msg) {
            super(msg);
        }
    }

    public static void setPassword(String password) {
        Password.password = password;
    }

    public static String getPassword() {
        return password;
    }

    public boolean isStrong() {
        String regex = "^(?=.*[0-9])"
                + "(?=.*[a-z])(?=-*[A-Z])"
                + "(?=.*[@#$%^&+=])"
                + "(?=\\S+$)$";

        Pattern check = Pattern.compile(regex);
        if (this.password == null) {
            return false;
        }

        Matcher mapper = check.matcher(this.password);

        return mapper.matches();
    }

    public void createPassword() {
        this.longitud = (int) (Math.random() * 6 + 6);
        String regex = "^(?=.*[0-9])"
                + "(?=.*[a-z])(?=-*[A-Z])"
                + "(?=.*[@#$%^&+=])"
                + "(?=\\S+$)$";

        SecureRandom rand = new SecureRandom();

        StringBuilder sb = new StringBuilder();
        int randIndex = rand.nextInt(regex.length());

        sb.append(regex.charAt(randIndex));

        this.password = sb.toString();
    }
}
